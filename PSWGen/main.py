import  random


chars = '+-/*!&$#?=@<>abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'

pswQuantity = input('Сколько нужно паролей? ')
pswLength = input('Длина пароля: ')

pswLength = int(pswLength)
pswQuantity = int(pswQuantity)

for n in range(pswQuantity):
    password =''
    for i in range(pswLength):
        password += random.choice(chars)
    print(password)
