from django.db import models

class Customer(models.Model):
    name = models.CharField(max_length=200, null=True, verbose_name = "ФИО")
    phone = models.CharField(max_length=200, null=True, verbose_name = "Телефон")
    email = models.EmailField(max_length=200, null=True)
    date_created = models.DateTimeField(auto_now_add=True, null=True, verbose_name = "Дата создания")
    address = models.CharField(max_length=200, null=True, verbose_name = "Адрес")
    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=200, null=True, verbose_name = "Наименование товара")
    cost = models.FloatField(null=True, verbose_name = "Цена")
    vendor = models.CharField(max_length=200, null=True, verbose_name = "Производитель")
    discount = models.IntegerField(default=0, verbose_name = "Скидка")
    date_created = models.DateTimeField(auto_now_add=True, null=True, verbose_name = "Дата создания")

    def __str__(self):
        return self.name

class Order(models.Model):
    customer = models.ForeignKey(Customer, null=True, on_delete= models.CASCADE, verbose_name = "Покупатель")
    product = models.ForeignKey(Product, null=True, on_delete= models.CASCADE, verbose_name = "Товар")
    date_created = models.DateTimeField(auto_now_add=True, null=True, verbose_name = "Дата создания")
    status = models.CharField(max_length=200, null=True,verbose_name = "Статус" , choices=(
        ('CANCELED','Отменен'),
        ('COMPLETED','Завершен'),
        ('REFUNDED','Возврат'),
        ('ASSEMBLING','Сборка'),
        ('AWAITING DEPARTURE','Ожидает отправления'),
        ('SENT', 'Отправлен'),
        ('PENDING PAYMENT','Ожидает оплаты'),
    ))

