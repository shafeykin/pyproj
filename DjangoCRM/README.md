# Django CRM

Django CRM, в которой вы можете создавать, обновлять или удалять товары, заказы и контакты. Также можете выполнять фильтрацию и поиск. Можете добавить задачи для напоминания.

+  Создание, обновление или удаление Товаров, Заказов и Контактов.
+  Поиск и фильтрация.
+  Список задач.

### Скриншоты

<table>
  <tr>
  <td align="center">
      <a href="https://github.com/nimadorostkar/DjangoCRM/blob/master/screenshots/login.png">
        <img src="screenshots/login.png" alt="Login Page">
      </a>
      <br />
      <p>Страница входа</p>
    </td>
    <td align="center">
      <a href="https://github.com/nimadorostkar/DjangoCRM/blob/master/screenshots/dashboard.png">
        <img src="screenshots/dashboard.png" alt="Dashboard">
      </a>
      <br />
      <p>Dashboard</p>
    </td>
    <td align="center">
      <a href="https://github.com/nimadorostkar/DjangoCRM/blob/master/screenshots/contacts.png">
        <img src="screenshots/contacts.png" alt="Contacts">
      </a>
      <br />
      <p>Контакты</p>
    </td>
    <td align="center">
      <a href="https://github.com/nimadorostkar/DjangoCRM/blob/master/screenshots/product.png">
        <img src="screenshots/product.png" alt="Products">
      </a>
      <br />
      <p>Товары</p>
    </td>
    <td align="center">
      <a href="https://github.com/nimadorostkar/DjangoCRM/blob/master/screenshots/tasks.png">
        <img src="screenshots/tasks.png" alt="Tasks">
      </a>
      <br />
      <p>Задачи</p>
    </td>
    </tr>
</table>

### Клонироть репозиторий

```
git clone <URL>
```

```
### Для начала выполните :

```
pip install django -r req.txt
```
```
python manage.py makemigrations
```
```
python manage.py migrate --run-syncdb
```
```
python manage.py runserver
```
