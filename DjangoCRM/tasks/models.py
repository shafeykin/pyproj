from django.db import models

# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200, null=True, verbose_name = "Задача")
    desc = models.CharField(max_length=300, null=True, verbose_name = "Описание")
    date_created = models.DateTimeField(auto_now_add=True, null=True, verbose_name = "Дата создания+")